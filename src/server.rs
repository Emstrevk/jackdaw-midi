use crate::midi::MidiNote;
use crate::midi::Output;
use crate::data::Note;
use crate::midi::MidiChannel;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::thread::sleep;
use std::time;
use std::collections::HashMap;
use midir::MidiOutput;
use std::io::stdin;
use std::collections::BTreeMap; // Sort by name usage

/**
   Main class: Handles the receiving, queueing and looping
       playback of MIDI notes into MIDI outputs via thread.
*/
pub struct MIDIServer {
    output_connections: Arc<Mutex<HashMap<String, Arc<Mutex<Output>>>>>,
    playing: bool,
    reset_requested: bool,
}

impl MIDIServer {
    pub fn new() -> Self {
        MIDIServer {
            output_connections: Arc::new(Mutex::new(HashMap::new())),
            playing: false,
            reset_requested: false,
        }
    }

    /*
       Queue a set of notes to the named output if it exists
    */
    pub fn receive_notes(
        this: Arc<Mutex<MIDIServer>>,
        output_key: &str,
        notes: Vec<Note>,
    ) {
        match this.lock().unwrap().output_connections.lock().unwrap().get_mut(output_key) {
            Some(output) => {
                let mut midi_vector = Vec::new();

                // Keep track of relative time between notes
                let mut time_reference = 0.0;

                // Convert notes to timestamped MIDI notes
                for note in &notes {
                    let new_midi_on = MidiNote::on(
                        *note,
                        time_reference,
                        MidiChannel::of(0), // NOte: channel is TODO
                        output_key,
                    );

                    time_reference += note.reserved_time().clone();

                    midi_vector.push(new_midi_on);
                }

                // Tell the output what's next
                output.lock().unwrap().queue(midi_vector);

                //println!("Queued {} notes for {}", notes.len(), output_key);
            }
            None => {
                println!("Ouput {} does not exist; notes thrown", output_key);
            }
        }
    }


    pub fn connect_all(&mut self) {
        let scanner = MidiOutput::new("Jackdaw_Scan").unwrap();
        let mut sorted_port_map = BTreeMap::new();

        for i in 0..scanner.port_count() {
            sorted_port_map.insert(scanner.port_name(i).unwrap(), i);
        }

        for (name, index) in sorted_port_map {
            println!(
                "Please name output port: {} (Blank = Skip)",
                name
            );

            let mut input = String::new();
            // TODO: Interactive at some point, for now rely on auto
            //stdin().read_line(&mut input).unwrap();
            let user_provided_name = input.trim().to_string();
            let mut final_name = user_provided_name.clone();
            if final_name.is_empty() {
                if name.contains(":") {
                    let colon_index = name.rfind(":").unwrap();
                    // TODO: Will panic if last char is ":"
                    final_name = String::from(&name[colon_index + 1..]);
                    final_name.retain(|c| !c.is_whitespace());
                } else {
                    final_name = name.clone();
                }
            }

            println!("Now available as: {}", final_name);

            // Establish the connection
            let reserved_out = MidiOutput::new(&final_name).unwrap();
            let new_connection = reserved_out.connect(index, &final_name).unwrap();

            // Create the output object for later note additions
            let connection_wrapper = Output::new(new_connection);

            self.output_connections.lock().unwrap().insert(
                final_name.to_string(),
                // Wrap it in a nice thread-safe ribbon
                Arc::new(Mutex::new(connection_wrapper)),
            );
        }
    }

    /*
       Attempt to create an output and save its reference for later playback
       Currently done interactively in the console.
    */
    pub fn create_output(&mut self, name_it: &str) -> Option<String> {
        // Register a virtual port with the name provided
        let midi_out = MidiOutput::new(&name_it).unwrap();

        // INteractive prompt: Select from existing outputs
        let output_device = match midi_out.port_count() {
            0 => {
                println!("No output port found");
                return None;
            }
            1 => {
                println!(
                    "Choosing the only available output port: {} for {}",
                    midi_out.port_name(0).unwrap(),
                    name_it
                );
                0
            }
            _ => {
                println!("\nAvailable output ports:");
                for i in 0..midi_out.port_count() {
                    println!("{}: {}", i, midi_out.port_name(i).unwrap());
                }
                println!("Please select output port for {}: ", name_it);

                let mut input = String::new();
                stdin().read_line(&mut input).unwrap();
                input.trim().parse().unwrap()
            }
        };

        // Establish the connection
        let new_connection = midi_out.connect(output_device, &name_it).unwrap();

        // Create the output object for later note additions
        let connection_wrapper = Output::new(new_connection);

        // Aaand save the reference.
        // Honestly I'm kinda scared these "ARC" references might
        //  just point to nothing since the scope ends, but
        //  if I recall correctly these references also say
        //  "Keep alive for as long as I exist"
        self.output_connections.lock().unwrap().insert(
            name_it.to_string(),
            // Wrap it in a nice thread-safe ribbon
            Arc::new(Mutex::new(connection_wrapper)),
        );

        Some(name_it.to_string())
    }

    pub fn panic_midi(&self) {
        for output in self.output_connections.lock().unwrap().values() {
            output.lock().unwrap().connection_mut()
                .send(&[crate::midi::ALL_NOTES_OFF]); // TODO: Whole method is clumsy and disrespects boundaries
        }
    }

    pub fn play_note(this: Arc<Mutex<MIDIServer>>, note: MidiNote, bpm: i32) {
        match this.lock().unwrap()
            .output_connections
            .lock()
            .unwrap()
            .get_mut(note.output_name())
        {
            Some(output) => {
                output
                    .lock()
                    .unwrap()
                    .connection_mut()
                    .send(&[note.channel_on_event().clone(), *note.tone() as u8, *note.velocity()])
                    .ok(); // Pretty sure this is big bad dangerousxx
                //println!("play_note leaving scope.");
                ()
            }
            None => {
                println!("ERROR: No such output: {}", note.output_name());
            }
        };

        thread::spawn(move || {
            let sleep_time =
                time::Duration::from_millis(
                    crate::midi::beats_to_milliseconds(
                        note.sustain_time().clone(),
                        bpm,
                    ) as u64
                );
            sleep(sleep_time);

            match this.lock().unwrap()
                .output_connections
                .lock()
                .unwrap()
                .get_mut(note.output_name())
            {
                Some(output) => {
                    output
                        .lock()
                        .unwrap()
                        .connection_mut()
                        .send(&[note.channel_off_event().clone(), *note.tone() as u8, *note.velocity()])
                        .ok();
                    ()
                }
                None => {
                    println!("ERROR: No such output: {}", note.output_name());
                }
            };

        });
    }

    pub fn force_reset(this: Arc<Mutex<MIDIServer>>) {
        println!("Force reset call received");
        for connection in this.lock().unwrap().output_connections.lock().unwrap().values() {
            connection.lock().unwrap().reset();
        }

        this.lock().unwrap().reset_requested = true;
    }

    // TODO: Modularize
    pub fn play(this: Arc<Mutex<MIDIServer>>, bpm: i32) {

        // Verify, in a limited scope, wether the server is
        // already playing (and only allow one at a time)
        {
            let mut self_lock = this.lock().unwrap();
            if self_lock.playing {
                //println!("Server already playing; ignoring play call.");
                return ();
            } else {
                self_lock.playing = true;
            }
        }

        println!("#! STARTING A BRAND NEW LOOP");

        thread::spawn(move || {
            // Play loop will loop forever
            loop {
                //println!("#### Entering server loop...");
                let mut last_played_note_time = 0.0;

                let mut notes: Vec<MidiNote> = Vec::new();

                // Iterate all values (arcs) in the output map,
                //  gather their notes into a combined vector
                // Note: Temporary lock, dropped after loop
                for (_, output_arc) in this.lock().unwrap().output_connections.lock().unwrap().clone() {
                    let mut output = output_arc.lock().unwrap();

                    // Ensure we're using the latest queued set
                    output.reset();
                    match output.current_set() {
                        Some(notevec) => notes.append(&mut notevec.to_vec()),
                        None => (),
                    }
                }

                // Sort combined vector by start_time
                notes.sort();

                //println!("Length of this loop: {}", notes.len());

                // Begin sending MIDI
                for note in notes.clone() {

                    // Calculate how long to sleep
                    let now = note.start_time();
                    let then_to_now = now - last_played_note_time;
                    let sleep_time =
                        time::Duration::from_millis(
                            crate::midi::beats_to_milliseconds(
                                then_to_now,
                                bpm,
                            ) as u64
                        );
                    last_played_note_time = note.start_time().clone();

                    // Sleep to sync actual time with virtual time
                    sleep(sleep_time);

                    // ... aaand send the actual signal via the connection.
                    //println!("Playing note {:?}", note);

                    // Grab a new, separate lock to play note
                    // Not keeping lock from before allows us
                    // to leave the reference unlocked during sleep
                    // NOTE: It is possible to lock the reference
                    //  through some other method during sleep,
                    //  potentially prolonging sleep call and causing
                    //  off-beat melody
                    MIDIServer::play_note(this.clone(), note, bpm);

                    // If flag has been set, interrup early
                    // TODO: Not tested
                    if this.lock().unwrap().reset_requested {
                        println!("RECEIVED!");
                        this.lock().unwrap().reset_requested = false;
                        this.lock().unwrap().panic_midi();
                        break;
                    }
                    //println!("Finished playing note.");
                }

                // Since sleep happens between notes, the final note needs to sleep out
                //  its reserved time to avoid going off-beat
                // THis is a bit of a smell indicating poor loop hygiene, but it does the job for now
                let final_note = notes.last().unwrap().clone();
                let sleep_time =
                    time::Duration::from_millis(
                        crate::midi::beats_to_milliseconds(
                            final_note.reserved_time().clone(),
                            bpm,
                        ) as u64
                    );
                // Sleep to sync actual time with virtual time
                sleep(sleep_time);

                //println!("Full play finished, resetting...")
            } // end loop
        }); // end thread
    }
}
