

const NOTE_ON_HEX: u8 = 0x90;
const NOTE_OFF_HEX: u8 = 0x80;
pub const ALL_NOTES_OFF: u8 = 0x7B;

#[derive(Debug, Clone)]
pub struct MidiChannel {
    zero_base_index: u8
}

impl MidiChannel {
    pub fn of(index: i32) -> Self {

        let final_index: i32;

        // num::clamp is an external crate; this is cheaper
        if index > 15 {
            println!(
                "Warning: Channel value {} is too high; will be clamped",
                index
            );
            final_index = 15
        } else if index < 0 {
            println!(
                "Warning: Channel value {} is too low; will be clamped",
                index
            );
            final_index = 0
        } else {
            final_index = index
        }

        MidiChannel {
            zero_base_index: final_index as u8
        }
    }

    /*
        Byte signatures for the midi message to send on or off to the channel
     */

    pub fn on_event(&self) -> u8 {
        NOTE_ON_HEX + self.zero_base_index
    }

    pub fn off_event(&self) -> u8 {
        NOTE_OFF_HEX + self.zero_base_index
    }

}

pub mod test {

    #[test]
    fn test_bpm_calc() {
        use crate::midi::beats_to_milliseconds;

        let bpm = 120;
        let beats = 4.0;

        // 4 * 60/120 = 2
        assert_eq!(2000.0, beats_to_milliseconds(beats, bpm));
    }

    #[test]
    fn test_hex_format_combo() {
        let note = crate::midi::MidiChannel::of(18);
        assert_eq!(15,note.zero_base_index);
        assert_eq!("0x9F", format!("{:#X}", note.on_event()));
        assert_eq!("0x8F", format!("{:#X}", note.off_event()));
    }

}


use core::cmp::Ordering;
use crate::data::Note;

#[derive(Debug, Clone)]
pub struct MidiNote {
    channel: MidiChannel,
    start_time: f32, // Was "time", u64
    sustain_time: f32, // Was "vtime", u64
    reserved_time: f32,
    tone: usize, // u8
    velocity: u8, // u8
    output_name: String
}

impl MidiNote {

    pub fn midi_velocity(source_note: Note) -> u8 {
        let ceiling = 127;
        let calc = *source_note.amplitude() * ceiling as f32;
        if calc > ceiling as f32 {
            ceiling
        } else {
            calc as u8
        }

    }

    pub fn on(
        source_note: Note,
        start_time: f32,
        channel: MidiChannel,
        output_name: &str
    ) -> Self {
        MidiNote {
            channel,
            start_time,
            sustain_time: *source_note.sustain_time(),
            reserved_time: *source_note.reserved_time(),
            tone: *source_note.tone(),
            velocity: MidiNote::midi_velocity(source_note),
            output_name: output_name.to_string()
        }
    }

    pub fn channel_on_event(&self) -> u8 {self.channel.on_event().clone()}
    pub fn channel_off_event(&self) -> u8 {self.channel.off_event().clone()}
    pub fn start_time(&self) -> &f32 {&self.start_time}
    pub fn sustain_time(&self) -> &f32 {&self.sustain_time}
    pub fn reserved_time(&self) -> &f32 {&self.reserved_time}
    pub fn tone(&self) -> &usize {&self.tone}
    pub fn velocity(&self) -> &u8 {&self.velocity}
    pub fn output_name(&self) -> &String {&self.output_name}

}

/*
    Sortability functions
 */

impl Ord for MidiNote {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.start_time < other.start_time {
            Ordering::Less
        } else if self.start_time > other.start_time {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for MidiNote {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for MidiNote {
    fn eq(&self, other: &Self) -> bool {
        self.start_time == other.start_time
    }
}

// This is the example from web
// Honestly there's a LOT of boilerplate for simple comparison stuff
impl Eq for MidiNote {}

pub fn beats_to_milliseconds(beats: f32, bpm: i32) -> f32 {
    beats * (60.0 / bpm as f32) * 1000.0
}


use midir::MidiOutputConnection;
/**
 *  Output source abstraction; handle
 *  mainly the queueing of notes to be sent for a connection
 *  TODO: Own mod
 */
pub struct Output {
    connection: MidiOutputConnection,
    current_set: Option<Vec<MidiNote>>,
    queued_set: Option<Vec<MidiNote>>,
}

impl Output {
    pub fn new(connection: MidiOutputConnection) -> Self {
        Output {
            connection,
            current_set: None,
            queued_set: None,
        }
    }

    pub fn current_set(&self) -> Option<Vec<MidiNote>> {
        match &self.current_set {
            Some(vector) => Some(vector.to_vec()),
            None => None,
        }
    }

    pub fn queued_set(&self) -> Option<Vec<MidiNote>> {
        match &self.queued_set {
            Some(vector) => Some(vector.to_vec()),
            None => None,
        }
    }

    /*
       Order a vector of notes to become current on next reset
       Note: By design overrides existing queues; there can only
       be one.
    */
    pub fn queue(&mut self, notes: Vec<MidiNote>) {
        self.queued_set = Some(notes);
    }

    /*
       "pop" the queue, transporting the queued list to current
    */
    pub fn reset(&mut self) {
        match &self.queued_set {
            Some(vector) => {
                self.current_set = Some(vector.to_vec());
                self.queued_set = None
            }
            None => (),
        }
    }

    pub fn connection_mut(&mut self) -> &mut MidiOutputConnection {
        &mut self.connection
    }
}
