use rocket_contrib::json::Json;
use rocket::State;
use std::sync::Arc;
use std::sync::Mutex;
use crate::server::MIDIServer;
use crate::data::Note;

// TODO: Receive all input as batch, allow merging of
// duplicates for same output

/**
 * API handles to provide a http->MidiServer abstraction
 */

#[get("/")]
pub fn about() -> &'static str {
    "Caw Caw!"
}

#[get("/play")]
pub fn play(
    midi: State<Arc<Mutex<MIDIServer>>>
) {
    MIDIServer::play(midi.clone(), 120);
}

#[get("/reset")]
pub fn force_reset(
    midi: State<Arc<Mutex<MIDIServer>>>
) {
    MIDIServer::force_reset(midi.clone());
}

#[post("/queue/<output_key>", format="json", data="<notes>")]
pub fn queue_notes(
    notes: Json<Vec<Note>>,
    output_key: String,
    midi: State<Arc<Mutex<MIDIServer>>>
) {
    let note_dese = notes.into_inner();
    MIDIServer::receive_notes(midi.clone(), &output_key, note_dese);
}
