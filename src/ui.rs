extern crate ncurses;

use ncurses::*;

pub fn ask(question: &str, options: Vec<&str>) -> String {
    loop {
        mvprintw(0, 0, question);
        let mut index = 0;
        for option in &options {
            mvprintw(index + 1, 0, &format!("{}. {}", index, option));
            index += 1;
        }

        let answer = getch();
        use core::char::from_u32;
        match from_u32(answer as u32).unwrap().to_digit(10) {
            Some(i) => {
                //clear();
                match options.get((i) as usize) {
                    Some(s) => return String::from(*s),
                    None => {
                        log("Listed numbers only!");
                    }
                }
            },
            None => {
                //clear();
                log("Please select a number");
            }
        }
    }
}

pub fn display(lines: Vec<&str>) {
    let mut index = 0;
    for line in lines {
        mvprintw(index, COLS() - line.len() as i32, line);
        index += 1;
    }
}

pub fn log(message: &str) {
    //clear();
    mvprintw(LINES() -1 , COLS() - message.len() as i32, message);
    refresh();
}

pub fn start() {
    // Create window
    initscr();

    // Ncurses print
    mvprintw(8, 5, "Putting this somewhere random");

    //printw("Press to close");
    refresh(); // Screen must be refreshed on change

    ask("If wood falls in tree?", vec!["See", "No see"]);

    endwin(); // Terminate
}
