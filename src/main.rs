#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

use std::sync::{Arc, Mutex};
//use crate::data::Note;
use crate::server::MIDIServer;
//use api::*;

pub mod data;
pub mod midi;
pub mod api;
pub mod server;
pub mod ui;

use ::std::*;

pub fn main() {

    //ui::start();

    let midi_server = MIDIServer::new();
    let midi_ref = Arc::new(Mutex::new(midi_server));

    {
        // http://localhost:8000/notes/queue/Drone
        midi_ref.lock().unwrap().connect_all();
    }

    rocket::ignite()
        .mount("/notes", routes![api::queue_notes, api::play, api::force_reset])
        .mount("/about", routes![api::about])
        .manage(midi_ref)
        .launch();

}
