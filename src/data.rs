
use serde::{Deserialize, Serialize};

/**
 * Re-implementation of Note from compose lib
 * The expected json unit
 * TODO: Example json payload
 */
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Note {
    tone: usize,
    sustain_time: f32,
    reserved_time: f32,
    amplitude: f32,
}

impl Note {
    pub fn tone(&self) -> &usize { &self.tone }
    pub fn sustain_time(&self) -> &f32 { &self.sustain_time }
    pub fn reserved_time(&self) -> &f32 { &self.reserved_time }
    pub fn amplitude(&self) -> &f32 { &self.amplitude }
}
