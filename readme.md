# DEPRECATED REPO
progress moved to: https://bitbucket.org/Emstrevk/jdw-midi-server/src/master/

# Jackdaw
Jackdaw is a MIDI messaging service written in Rust.

### Features
- Receives and plays MIDI notes via REST API
- Looping received MIDI note arrays to enable live coding via payload
- Scans, connects to and maintains an indexed list of available MIDI outputs
